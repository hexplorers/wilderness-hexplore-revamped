all: Wilderness-Hexplore-Revamped.pdf

MD=Wilderness-Hexplore-Revamped.md

%.pdf: ${MD} license.md Makefile
	pandoc $< license.md -o $@

view: all
	zathura *.pdf

clean:
	rm -f *~

realclean: clean
	rm -f *.pdf
